import 'dart:math';
import 'character.dart';
import 'game.dart';

abstract class Item {
  String name = "No Item";
  Item(this.name);
  Character? player1;
  Character? player2;
  Character? player;
  int positionTrap = 0;
  int damage = 0;
  Character? turn;

  void setPlayer(Character player) {
    this.player = player;
  }

  void showMessage() {
    print("");
  }
}

class Rock extends Item {
  Rock(super.name);

  void bounce(Character player) {
    player.setDecreasePosiotion(2);
  }

  @override
  void showMessage() {
    print("Use Rock Bounce Opponent");
  }
}

class Bandage extends Item {
  Bandage(super.name);
  void wound(Character player) {
    player.setHealthPointIncrease(2);
  }

  @override
  void showMessage() {
    print("Use Bandage Wound Yourself");
  }
}

class Sniper extends Item {
  Sniper(super.name);
  void shoot(Character player) {
    player.setHealthPointDecrease(2);
  }

  @override
  void showMessage() {
    print("Use Sniper Shoot Opponent");
  }
}


