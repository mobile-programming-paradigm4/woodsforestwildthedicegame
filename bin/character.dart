import 'dart:math';

abstract class Character {
  String name = "No Name";
  int healthPoint = 0;
  String item = "No Item";
  int position = 1;
  int historyDice = 0;

  Character(this.name,this.healthPoint,this.item);
  void walk(int move){
    position+=move;
  }
  int getPostion(){
    return position;
  }
  void getDamage(int i){
    healthPoint-=i;
  }
  void setHistoryDice(int historyDice){
    this.historyDice = historyDice;
  }
  int getHistoryDice(){
    return historyDice;
  }
  int getHealthPoint(){
    return healthPoint;
  }
  String getItem(){
    return item;
  }
  void setDecreasePosiotion(int p){
    position-=p;
  }

  void setHealthPointIncrease(int p){
    healthPoint+=p;
  }
  
  void setHealthPointDecrease(int p){
    healthPoint-=p;
  }

  void rest(int p) {
    healthPoint+=p;
    print("You Rest get 1 HealthPoint");
  }
}

class Explorer extends Character {
  Explorer():super("Explorer",5,"Log");

}

class Doc extends Character{
  Doc():super("Doc",7,"Bandage");

}

class Hitman extends Character{
  Hitman():super("Hitman",4,"Sniper");

}

