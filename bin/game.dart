import 'dart:io';
import 'character.dart';
import 'dice.dart';
import 'item.dart';
import 'mapgame.dart';
import 'trap.dart';

class Game {
  Character? player1;
  Character? player2;
  MapGame? map;
  int turn = 1;
  int sizeMap = 0;
  Event trap = Event("");

  Game() {
    this.player1;
    this.player2;
    this.map = MapGame();
  }

  void start() {
    selectCharacter1();
    selectCharacter2();
  }

  void selectCharacter1() {
    print("Choose SizeMap this Game : input int number");
    sizeMap = int.parse(stdin.readLineSync()!);
    print('Player1 : Select Character : input int number');
    print('1 : Explorer, 2 : Doc, 3 : Hitman');
    String? selectCharacter1 = stdin.readLineSync();
    if (selectCharacter1 == "1") {
      Explorer explorer = Explorer();
      player1 = explorer;
      print("Name: ${explorer.name}");
      print("HealthPoint: ${explorer.healthPoint}");
      print("Item: ${explorer.item}");
      print("-----------------");
    } else if (selectCharacter1 == "2") {
      Doc doc = Doc();
      player1 = doc;
      print("Name: ${doc.name}");
      print("HealthPoint: ${doc.healthPoint}");
      print("Item: ${doc.item}");
      print("-----------------");
    } else if (selectCharacter1 == "3") {
      Hitman hitman = Hitman();
      player1 = hitman;
      print("Name: ${hitman.name}");
      print("HealthPoint: ${hitman.healthPoint}");
      print("Item: ${hitman.item}");
      print("-----------------");
    }
    // map!.setPlayer1(player1!);
  }

  void selectCharacter2() {
    print('Player2 : Select Character : input int number');
    print('1 : Explorer, 2 : Doc, 3 : Hitman');
    String? selectCharacter2 = stdin.readLineSync();
    if (selectCharacter2 == "1") {
      Explorer explorer = Explorer();
      player2 = explorer;
      print("Name: ${explorer.name}");
      print("HealthPoint: ${explorer.healthPoint}");
      print("Item: ${explorer.item}");
      print("-----------------");
    } else if (selectCharacter2 == "2") {
      Doc doc = Doc();
      player2 = doc;
      print("Name: ${doc.name}");
      print("HealthPoint: ${doc.healthPoint}");
      print("Item: ${doc.item}");
      print("-----------------");
    } else if (selectCharacter2 == "3") {
      Hitman hitman = Hitman();
      player2 = hitman;
      print("Name: ${hitman.name}");
      print("HealthPoint: ${hitman.healthPoint}");
      print("Item: ${hitman.item}");
      print("-----------------");
    }
    // map!.setPlayer2(player2!);
  }

  void welcome() {
    var welcome = Runes(
        '''\n
      ██     ██ ███████ ██       ██████  ██████  ███    ███ ███████     ████████  ██████      ██     ██ ██ ██      ██████  
      ██     ██ ██      ██      ██      ██    ██ ████  ████ ██             ██    ██    ██     ██     ██ ██ ██      ██   ██ 
      ██  █  ██ █████   ██      ██      ██    ██ ██ ████ ██ █████          ██    ██    ██     ██  █  ██ ██ ██      ██   ██ 
      ██ ███ ██ ██      ██      ██      ██    ██ ██  ██  ██ ██             ██    ██    ██     ██ ███ ██ ██ ██      ██   ██ 
       ███ ███  ███████ ███████  ██████  ██████  ██      ██ ███████        ██     ██████       ███ ███  ██ ███████ ██████                                                                                                                                                                                                                                       
    ''');
    print(new String.fromCharCodes(welcome));
  }

  void action() {
    print("Choose Action player $turn");
    print("Item Roll or Wound : input Item for use "
        'item'
        " Roll for "
        'walk'
        " Wound for resotre 1 healthPoint");
    String? action = stdin.readLineSync();
    if (turn == 1) {
      if (action == "Item") {
        if (player1?.getItem() == "Rock") {
          var event = Rock("rock");
          event.bounce(player2!);
        } else if (player1?.getItem() == "Bandage") {
          var event = Bandage("bandage");
          event.wound(player1!);
        } else if (player1?.getItem() == "Sniper") {
          var event = Sniper("sniper");
          event.shoot(player2!);
        }
      } else if (action == "Roll") {
        WalkDice walkDice = WalkDice();
        int value = walkDice.walkDice();
        player1!.walk(value);
        player1!.setHistoryDice(value);
        // print(walkDice.walkDice());
      } else if (action == "Wound") {
        player1?.rest(1);
      }
    } else {
      if (action == "Item") {
        //String? item = stdin.readLineSync();
        if (player2?.getItem() == "Rock") {
          var event = Rock("rock");
          event.bounce(player1!);
        } else if (player2?.getItem() == "Bandage") {
          var event = Bandage("bandage");
          event.wound(player2!);
        } else if (player2?.getItem() == "Sniper") {
          var event = Sniper("sniper");
          event.shoot(player1!);
        }
      } else if (action == "Roll") {
        WalkDice walkDice = WalkDice();
        int value = walkDice.walkDice();
        player2!.walk(value);
        player2!.setHistoryDice(value);
        // print(walkDice.walkDice());
      } else if (action == "Wound") {
        player2?.rest(1);
      }
    }
  }

  void switchTurn() {
    if (turn == 1) {
      turn = 2;
    } else {
      turn = 1;
    }
  }

  void clearScreen() {
    print("\x1B[2J\x1B[0;0H");
  }

  void status() {
    if (turn == 1) {
      print("Player2: ");
      print("Name: ${player2?.name}");
      print("HealthPoint: ${player2?.healthPoint}");
      print("Item: ${player2?.item}");
      print("LastestRoll: ${player2!.getHistoryDice()}");
      print("position: ${player2?.position}");
      print("-----------------");
      print("Player1: ");
      print("Name: ${player1?.name}");
      print("HealthPoint: ${player1?.healthPoint}");
      print("Item: ${player1?.item}");
      print("LastestRoll: ${player1!.getHistoryDice()}");
      print("position: ${player1?.position}");
      print("-----------------");
    } else {
      print("Player1: ");
      print("Name: ${player1?.name}");
      print("HealthPoint: ${player1?.healthPoint}");
      print("Item: ${player1?.item}");
      print("LastestRoll: ${player1!.getHistoryDice()}");
      print("position: ${player1?.position}");
      print("-----------------");
      print("Player2: ");
      print("Name: ${player2?.name}");
      print("HealthPoint: ${player2?.healthPoint}");
      print("Item: ${player2?.item}");
      print("LastestRoll: ${player2!.getHistoryDice()}");
      print("position: ${player2?.position}");
      print("-----------------");
    }
  }

  void event() {
    if (turn == 1) {
      if (trap.randomTrap() == 1) {
        var spear = Spear("spear");
        spear.attack(player1!);
        spear.showMessage();
      } else if (trap.randomTrap() == 2) {
        var beast = Beast("beast");
        beast.attack(player1!);
        beast.showMessage();
      } else if (trap.randomTrap() == 3) {
        var beast = Timber("timber");
        beast.bounce(player2!);
        beast.showMessage();
      } else if (trap.randomTrap() == 4) {
        var beast = Medkit("medkit");
        beast.wound(player1!);
        beast.showMessage();
      } else {
        var beast = SafePoint("safePoint");
        beast.showMessage();
      }
    } else if (turn == 2) {
      if (trap.randomTrap() == 1) {
        var spear = Spear("spear");
        spear.attack(player2!);
        spear.showMessage();
      } else if (trap.randomTrap() == 2) {
        var beast = Beast("beast");
        beast.attack(player2!);
        beast.showMessage();
      } else if (trap.randomTrap() == 3) {
        var beast = Timber("timber");
        beast.bounce(player1!);
        beast.showMessage();
      } else if (trap.randomTrap() == 4) {
        var beast = Medkit("medkit");
        beast.wound(player2!);
        beast.showMessage();
      } else {
        var beast = SafePoint("safePoint");
        beast.showMessage();
      }
    }
  }

  bool isFinish() {
    if (player1!.getPostion() >= sizeMap || player2!.getPostion() >= sizeMap) {
      return true;
    }
    return false;
  }

  void showWin() {
    if (player1!.getPostion() > player2!.getPostion()) {
      var Win1 = Runes(
          '''\n          
          ██████  ██       █████  ██    ██ ███████ ██████       ██     ██     ██ ██ ███    ██ ██ ██ ██ 
          ██   ██ ██      ██   ██  ██  ██  ██      ██   ██     ███     ██     ██ ██ ████   ██ ██ ██ ██ 
          ██████  ██      ███████   ████   █████   ██████       ██     ██  █  ██ ██ ██ ██  ██ ██ ██ ██ 
          ██      ██      ██   ██    ██    ██      ██   ██      ██     ██ ███ ██ ██ ██  ██ ██          
          ██      ███████ ██   ██    ██    ███████ ██   ██      ██      ███ ███  ██ ██   ████ ██ ██ ██                                          
    ''');
      print(new String.fromCharCodes(Win1));
    } else {
      var win2 = Runes(
          '''\n          
        ██████  ██       █████  ██    ██ ███████ ██████      ██████      ██     ██ ██ ███    ██ ██ ██ ██ 
        ██   ██ ██      ██   ██  ██  ██  ██      ██   ██          ██     ██     ██ ██ ████   ██ ██ ██ ██ 
        ██████  ██      ███████   ████   █████   ██████       █████      ██  █  ██ ██ ██ ██  ██ ██ ██ ██ 
        ██      ██      ██   ██    ██    ██      ██   ██     ██          ██ ███ ██ ██ ██  ██ ██          
        ██      ███████ ██   ██    ██    ███████ ██   ██     ███████      ███ ███  ██ ██   ████ ██ ██ ██                                                                                                                                                                                                                                                                                                                                                                                                                               
    ''');
      print(new String.fromCharCodes(win2));
    }
  }

  void checkHealthPoint() {
    if (player1!.getHealthPoint() <= 0) {
      player1!.position = 1;
      print("Player 1 Seriously Injured!! Return to Start Point");
    } else if (player2!.getHealthPoint() <= 0) {
      player2!.position = 1;
      print("Player 2 Seriously Injured!! Return to Start Point");
    }
  }

}

void main(List<String> arguments) {
  Game game = Game();
  game.welcome();
  game.start();
  while (true) {
    game.status();
    game.action();
    game.clearScreen();
    game.status();
    game.event();
    game.switchTurn();
    if (game.isFinish()) {
      game.clearScreen();
      game.showWin();
      game.status();
      break;
    }
  }
}
