import 'dart:math';

import 'character.dart';
import 'game.dart';

class Event {
  String? name;
  Event(this.name);
  Character? player1;
  Character? player;
  Character? player2;
  int positionTrap = 0;
  int damage = 0;
  //Character? turne;
  Game? turn;

  int randomTrap() {
    return positionTrap = Random().nextInt(10) + 1;
  }

  void setPlayer(Character player) {
    this.player = player;
  }

  void showMessage() {
    print("");
  }
}

class Spear extends Event implements Attack {
  Spear(super.name);
  @override
  void attack(Character player) {
    player.getDamage(1);
  }

  @override
  void showMessage() {
    print("You Found Spear Trap");
  }
}

class Beast extends Event with Attack {
  Beast(super.name);
  @override
  void attack(Character player) {
    player.getDamage(3);
  }

  @override
  void showMessage() {
    print("You Found Beast Trap");
  }
}

class Timber extends Event {
  Timber(super.name);

  void bounce(Character player) {
    player.setDecreasePosiotion(2);
  }

  @override
  void showMessage() {
    print("You Found Timber Trap");
  }
}

class Medkit extends Event {
  Medkit(super.name);

  void wound(Character player) {
    player.setHealthPointIncrease(2);
  }

  @override
  void showMessage() {
    print("You Found Medkit");
  }
}

class SafePoint extends Event {
  SafePoint(super.name);
  
  @override
  void showMessage() {
    print("You Safe");
  }
}

abstract class Attack {
  void attack(Character player) {}
}
