import 'dart:math';

abstract class Dice{
  String face;
  Dice(this.face);
} 

class WoundDice extends Dice{
  WoundDice() : super('Dice have 2 face');
  
  int woundDice(){
    return Random().nextInt(2)+1;
  }
}

class BounceDice extends Dice{
  BounceDice() : super('Dice have 10 face');

  int bounceDice(){
    return Random().nextInt(10)+1;
  }
}

class WalkDice extends Dice{
  WalkDice(): super('Dice have 6 face');
  
  int walkDice(){
    return Random().nextInt(6)+1;
  }
}